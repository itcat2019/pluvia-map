import { combineReducers } from 'redux';

const reducerName = '[reducer:/screen/map]'

export const START_CREATE_MARKER = `${reducerName} Start Creating Marker`;
export const END_CREATE_MARKER = `${reducerName} End Creating Marker`;
export const TOGGLE_LOCATION_PICKER = 'MAP_SCREEN_TOGGLE_LOCATION_PICKER';
export const ADD_MARKER = `${reducerName} Add Local Marker (reflect firestore change)`;
export const MODIFY_MARKER = `${reducerName} Modify Local Marker (reflect firestore change)`;
export const REMOVE_MARKER = `${reducerName} Remove Local Marker (reflect firestore change)`;
export const SHOW_INFOWINDOW = `${reducerName} Show Marker InfoWindow`;
export const CLOSE_INFOWINDOW = `${reducerName} Close Marker InfoWindow`;

// TODO: finish the function in MapScreen
export const START_EDIT_MARKER = `${reducerName} Start Editing Marker`;
export const END_EDIT_MARKER = `${reducerName} End Editing Marker`;


const mapInitialState = {
  // flag to represent the process of creating the marker
  // at this state the locationPicker is dropped but not yet save
  // also user is choosing icon and inputing texts
  // Map is rendering the location picker, and the control box is shown to key in details.
  // the details is not yet save to database though.
  // when save, this flag should turn back off.
  creatingMarker: false,
  // when creating marker, a Google Map Pin is rendered to represent the to-be marker position
  // those coordinates are saved in this variable
  pickerPosition: null,
  // --------- end of creatingMarker ---------

  // similar to creatingMarker, represent the state of updating but not yet save
  // markerId will be stored in this variable
  editingMarker: false,
  // store the dragged marker position before save button is hit
  stagingMarkerPosition: null,

  // stores all the rendering markers
  // markerId serve as key to reduce lookup time
  markers: {},
  // the marker that user chosen to see the InfoWindow
  showMarkerId: null,
}

function mapScreenReducer(state = mapInitialState, action) {
  switch (action.type) {
    case START_CREATE_MARKER:
      return {
        ...state,
        creatingMarker: true,
        editingMarker: null,
        pickerPosition: action.pickerPosition
      };
    case END_CREATE_MARKER:
      return {
        ...state,
        creatingMarker: false,
        editingMarker: null,
        pickerPosition: null
      };

    case ADD_MARKER:
    case MODIFY_MARKER:
      return {
        ...state,
        markers: {
          ...state.markers,
          [action.marker.markerId]: action.marker,
        },
      };
    case REMOVE_MARKER:
      return {
        ...state,
        markers: {
          ...state.markers,
          [action.marker.markerId]: undefined,
        },
        // [action.marker.markerId]: undefined,
      };

    // control marker infowindow
    case SHOW_INFOWINDOW:
      return {
        ...state,
        showMarkerId: action.markerId,
      };
    case CLOSE_INFOWINDOW:  // TODO: change to action type
      return {
        ...state,
        showMarkerId: null,
      };

    // marker editing
    case START_EDIT_MARKER:
      return {
        ...state,
        creatingMarker: false,
        editingMarker: action.markerId,
      }
    case END_EDIT_MARKER:
      return {
        ...state,
        creatingMarker: false,
        editingMarker: null,
      };
    default:
      return state;
  }
}

const screenReducer = combineReducers({
  map: mapScreenReducer,
})

export default screenReducer;