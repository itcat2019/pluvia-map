import React, { Component } from 'react';

import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';

import Fab from '@material-ui/core/Fab';

import CodeIcon from '@material-ui/icons/Code';
import HomeIcon from '@material-ui/icons/Home';

import GitHubCircleIcon from 'mdi-material-ui/GithubCircle';

import EmptyState from '../../layout/EmptyState/EmptyState';
import MapCard from '../../components/MapCard';
import { db } from '../../firebase';



const styles = (theme) => ({
  container: {
    margin: '40px auto 0',
    maxWidth: '800px',
  },
  card: {
    position: 'relative',
    marginBottom: 20,
  },
});

class HomeScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      maps: [ ],
    };
  }

  componentDidMount() {
    // TODO: change this to streaming later
    // consider shallow streaming
    db.collection('maps').get()
    .then( snapshot => {
      this.setState({ 
        maps: snapshot.docs.map(doc => ({ 
          mapId: doc.id, 
          ...doc.data(),
        })), 
      });
    });
  }

  render() {
    // Styling
    const { classes } = this.props;

    // Properties
    const { isSignedIn, title } = this.props;

    return (
      <Grid container className={classes.container}>
        {this.state.maps.map( (map) => (
          <Grid item xs={12} key={map.mapId} className={classes.card}>
            <MapCard map={map} />
          </Grid>
        ))}
      </Grid>
    );
  }
}

HomeScreen.propTypes = {
  classes: PropTypes.object.isRequired,

  isSignedIn: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired
};

export default withStyles(styles)(HomeScreen);