import React from 'react';
import { connect } from 'react-redux';
import firebase from 'firebase/app';
import { withStyles } from '@material-ui/core/styles';
import GoogleMap from '../components/GoogleMap';
import MarkerEditPanel from '../components/MarkerEditPanel';
import { db } from '../firebase';
import { END_CREATE_MARKER, ADD_MARKER, MODIFY_MARKER, REMOVE_MARKER, END_EDIT_MARKER } from '../redux/reducers/screenReducer';


const styles = (theme) => ({
  map: {
    height: 'calc(100vh - 64px)',
  },
  controlRight: {
    backgroundColor: 'rgba(255,255,255,0.5)',
    position: 'fixed',
    height: 'calc(100vh - 64px)',
    width: '40%',
    maxWidth: 400,
    right: 0,
    top: 64,
  },
  controlBottom: {
    backgroundColor: 'rgba(255,255,255,0.5)',
    position: 'fixed',
    height: '40%',
    width: '100%',
    bottom: 0,
  },
  iconCard: {
    cursor: 'pointer',
    marginTop: 20,
    width: 40,
    height: 40,
    display: 'flex',
    alignContent: 'center',
    justifyContent: 'center',
  },
  marginTopItem: {
    marginTop: 30,
  },

  rightIcon: {
    marginRight: theme.spacing(1),
  },
  button: {
    margin: theme.spacing(1),
  },
});

const ICONS = [
  {
    mdIcon: 'pets',
    color: null,
  }, {
    mdIcon: 'android',
    color: null,
  }, {
    mdIcon: 'error',
    color: null,
  }, {
    mdIcon: 'star',
    color: null,
  }, {
    mdIcon: 'block',
    color: null,
  }
];

class MapScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mapId: window.location.pathname.split('/').filter(p => p !== '').pop(),
      map: null,  // to be set after the db call in componentDidMount
      unsubscribeMarkers: undefined,
    };
  }

  componentDidMount() {
    const { mapId } = this.state;
    const unsubscribeMarkers = db.collection('maps').doc(mapId).collection('markers')
      .orderBy('createdAt', 'desc').limit(100)
      .onSnapshot(snapshot => {
        snapshot.docChanges().forEach(change => {
          const data = change.doc.data();
          const marker = {
            markerId: change.doc.id,
            latLng: { lat: data.position._lat, lng: data.position._long },
            ...data,
          };
          // TODO: can send change.type as action.type and let reducer do the switch
          switch (change.type) {
            case 'added':
              this.props.addMarker(marker); break;
            case 'modified':
              this.props.modifyMarker(marker); break;
            case 'removed':
              this.props.removeMarker(marker); break;
          }
        });
      });
    this.setState({ unsubscribeMarkers });

    db.collection('maps').doc(this.state.mapId).get()
      .then(doc => {
        this.setState({
          map: { mapId: doc.id, ...doc.data() }
        });
      });
  }

  componentWillUnmount() {
    this.state.unsubscribeMarkers && this.state.unsubscribeMarkers();
  }

  clearCreate = () => {
    this.props.unsetLocationPicker();
  }

  clearEdit = () => {
    this.props.clearEdit();
  }

  saveCreate = (values) => {

    const { mapId, title, description, emojiIconId } = values;
    const picker = this.props.pickerPosition;
    const timestamp = new firebase.firestore.Timestamp.now();

    db.collection('maps').doc(mapId).collection('markers')
      .add({
        title: title,
        description: description,
        emojiIconId: emojiIconId,
        color: null,
        position: new firebase.firestore.GeoPoint(picker.lat(), picker.lng()),
        createdAt: timestamp,
        updatedAt: timestamp,
      }).then((docRef) => {
        console.log('create marker success. id: ', docRef.id);
        this.clearCreate();
      }).catch(error => {
        console.error('create marker error', error);
      });
  }

  saveEdit = (values) => {

    const { mapId, markerId, title, description, emojiIconId } = values;
    const timestamp = new firebase.firestore.Timestamp.now();

    db.collection('maps').doc(mapId).collection('markers').doc(markerId)
      .update({
        title: title,
        description: description,
        emojiIconId: emojiIconId,
        color: null,
        updatedAt: timestamp
      }).then((docRef) => {
        console.log('update marker success. id: ', markerId);
        this.clearEdit();
      }).catch(error => {
        console.error('update marker error', error);
      });
  }

  render() {
    const { classes, creatingMarker, editingMarker } = this.props;
    const { map, mapId } = this.state;
    return (
      <div>
        <div className={classes.map}>
          {map && <GoogleMap defaultZoom={map.defaultZoom} defaultCenter={{ lat: map.defaultCenter._lat, lng: map.defaultCenter._long }} />}
        </div>

        {/* Control Box for creating marker */}
        {!editingMarker && creatingMarker && <MarkerEditPanel mapId={mapId} saveAction={this.saveCreate} cancelAction={this.clearCreate} />}
        {editingMarker && <MarkerEditPanel mapId={mapId} markerId={editingMarker} saveAction={this.saveEdit} cancelAction={this.clearEdit} />}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  creatingMarker: state.screen.map.creatingMarker,
  editingMarker: state.screen.map.editingMarker,
  pickerPosition: state.screen.map.pickerPosition,
});
const mapDispatchToProps = (dispatch) => ({
  unsetLocationPicker: () => dispatch({ type: END_CREATE_MARKER }),
  addMarker: (marker) => dispatch({ type: ADD_MARKER, marker }),
  modifyMarker: (marker) => dispatch({ type: MODIFY_MARKER, marker }),
  removeMarker: (marker) => dispatch({ type: REMOVE_MARKER, marker }),
  clearEdit: () => dispatch({ type: END_EDIT_MARKER }),
});
export default connect(mapStateToProps, mapDispatchToProps)((withStyles(styles)(MapScreen)));