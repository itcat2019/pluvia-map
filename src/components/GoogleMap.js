import React from 'react';
import { connect } from 'react-redux';
import { GoogleMap, Marker as GoogleMarker } from 'react-google-maps';
import { withGoogleMap, withScriptjs } from 'react-google-maps';
import { compose, withProps, withStateHandlers } from 'recompose';
import store from '../redux/store';
import { START_CREATE_MARKER } from '../redux/reducers/screenReducer';
import Marker from './Marker';

// reference: https://stackblitz.com/edit/react-umnzy4?file=MapContainer.js

const MapComponent = compose(
  // for location picker
  withStateHandlers(() => ({/*empty init state*/}), {
    onMapClick: ({ isPickerShown }) => (e) => store.dispatch({
      type: START_CREATE_MARKER,
      pickerPosition: e.latLng,
    })
  }),
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${process.env.REACT_APP_GOOGLE_MAP_API_KEY}`,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `100%` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) =>
  <GoogleMap
    defaultZoom={15}
    defaultCenter={props.defaultCenter} 
    onClick={props.onMapClick}
  >
    {/* location picker */}
    {props.isPickerShown && <GoogleMarker position={props.pickerPosition} />}

    {/* user markers */}
    {Object.entries(props.markers)
      .map( item => item[1] )
      .map( marker => marker && <Marker key={marker.markerId} marker={marker}/> )}

  </GoogleMap>
)

const mapStateToProps = (state) => ({
  isPickerShown: state.screen.map.creatingMarker,
  pickerPosition: state.screen.map.pickerPosition,
  markers: state.screen.map.markers,
});
export default connect(mapStateToProps)(MapComponent);