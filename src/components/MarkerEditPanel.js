import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Card, CardHeader, CardContent, Grid, Button, TextField } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import { Picker, emojiIndex } from 'emoji-mart';
import { withFormik } from "formik";
import { every, isEmpty } from 'lodash';
import { db } from '../firebase';

import 'emoji-mart/css/emoji-mart.css';

// TODO: this function is deprecrating. remove it soon.
import withWidth from '@material-ui/core/withWidth';

const useStyles = makeStyles(theme => ({
    map: {
        height: 'calc(100vh - 64px)',
    },
    controlRight: {
        backgroundColor: 'rgba(255,255,255,0.8)',
        position: 'fixed',
        height: 'calc(100vh - 64px)',
        width: '40%',
        maxWidth: 400,
        right: 0,
        top: 64,
    },
    controlBottom: {
        backgroundColor: 'rgba(255,255,255,0.8)',
        position: 'fixed',
        height: '40%',
        width: '100%',
        bottom: 0,
    },
    iconCard: {
        cursor: 'pointer',
        marginTop: 20,
        width: 40,
        height: 40,
        display: 'flex',
        alignContent: 'center',
        justifyContent: 'center',
    },
    marginTopItem: {
        marginTop: 30,
    },
    rightIcon: {
        marginLeft: theme.spacing(1),
    },
    button: {
        margin: theme.spacing(1),
    },
    iconButton: {
        marginTop: 30,
        margin: theme.spacing(1),
        fontSize: '2rem',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
}));


const InnerForm = ({ width, values, errors, handleChange, handleSubmit, handleCancel, isSubmitting, setFieldValue, dirty }) => {
    const classes = useStyles();

    const [shownIconPicker, setShownIconPicker] = useState(false);
    const [layers, setLayers] = useState([]);

    useEffect(() => {
        db.collection('layers').orderBy('label', 'asc').limit(100).get()
            .then(snapshot => {
                setLayers(snapshot.docs.map(doc => ({
                    layerId: doc.id,
                    ...doc.data(),
                })));
            });
    }, []);

    const openIconPicker = () => {
        setShownIconPicker(prev => ({ shownIconPicker: !prev.shownIconPicker }));
    }

    const updateIcon = (emoji) => {
        setFieldValue('emojiIconId', emoji.id);
        setShownIconPicker(false);
    }

    let emojiIcon = null;
    if (values.emojiIconId) {
        const foundEmoji = emojiIndex.search(values.emojiIconId);
        if (foundEmoji.length > 0) {
            emojiIcon = foundEmoji[0];
        }
    }

    const formDisabled = isSubmitting || !dirty || isEmpty(values) || every(values, property => isEmpty(property));
    return <Card className={width === 'xs' ? classes.controlBottom : classes.controlRight}>
        <CardHeader variant="h4" title={(values.mapId && values.markerId) ? "Edit Marker" + values.title : "Create Marker"} />
        <CardContent>
            <form onSubmit={handleSubmit} noValidate autoComplete="off">
                <Grid container spacing={3}>
                    <Grid item xs={12} >
                        <FormControl className={classes.formControl} error={!isEmpty(errors.layer)}>
                            <InputLabel htmlFor="layer">Nature</InputLabel>
                            <Select
                                value={values.layer}
                                onChange={handleChange}
                                inputProps={{
                                    name: 'layer',
                                    id: 'layer',
                                }}
                            >
                                {
                                    layers.map(l => <MenuItem key={l.layerId} value={l.value}>{l.label}</MenuItem>)
                                }
                            </Select>
                            <FormHelperText>{errors.layer}</FormHelperText>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} >
                        <Grid>
                            <FormControl fullWidth error={!isEmpty(errors.emojiIconId)}>
                                <InputLabel>Pick one icon</InputLabel>
                                <FormHelperText>{errors.emojiIconId}</FormHelperText>
                            </FormControl>
                        </Grid>
                        <Grid>
                            <Button variant="contained" className={classes.iconButton} onClick={openIconPicker}>
                                {values.emojiIconId && emojiIcon.native}
                                {!values.emojiIconId && " - "}
                            </Button>
                        </Grid>
                        {
                            shownIconPicker &&
                            <Grid>
                                <Picker onSelect={updateIcon} />
                            </Grid>
                        }
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label="Marker Title" className={classes.marginTopItem}
                            name='title'
                            value={values.title}
                            onChange={handleChange}
                            error={!isEmpty(errors.title)}
                            helperText={errors.title}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label="Description" className={classes.marginTopItem}
                            name='description'
                            value={values.description}
                            onChange={handleChange}
                            error={!isEmpty(errors.description)}
                            helperText={errors.description}
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Grid container direction="row" justify="flex-end" alignItems="center" className={classes.marginTopItem}>
                            <Button variant="contained" color="secondary" className={classes.button} onClick={handleCancel}>
                                Cancel
                                <CancelIcon className={classes.rightIcon} />
                            </Button>
                            <Button type="submit" variant="contained" color="primary" className={classes.button} disabled={formDisabled}>
                                Save
                                <SaveIcon className={classes.rightIcon} />
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </form>
        </CardContent>
    </Card>;
}

const Form = withFormik({
    enableReinitialize: true,
    mapPropsToValues: props => props.model,
    validate: values => {
        const errors = {};
        if (!values.layer) {
            errors.layer = "Required";
        }
        if (!values.emojiIconId) {
            errors.emojiIconId = "Required";
        }
        return errors;
    },
    handleSubmit: (values, formikBag) => formikBag.props.handleSubmit(values, formikBag)
})(InnerForm);

const MarkerEditPanel = ({ mapId, markerId, saveAction, cancelAction }) => {
    const defaultValue = { mapId, title: '', description: '', layer: '', emojiIconId: '' };

    const [markerData, setMarkerData] = useState(defaultValue);

    useEffect(() => {
        if (mapId && markerId) {
            db.collection('maps').doc(mapId).collection('markers').doc(markerId).get()
                .then(doc => {
                    setMarkerData(
                        Object.assign({}, defaultValue, {
                            mapId: mapId,
                            markerId: doc.id,
                            ...doc.data()
                        })
                    );
                });
        }
    }, [mapId, markerId]);

    return <Form model={markerData}
        handleSubmit={saveAction}
        handleCancel={cancelAction} />;
}
export default withWidth()(MarkerEditPanel);