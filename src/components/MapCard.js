import { Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { Card, CardMedia, CardContent } from '@material-ui/core';
import FavoriteIcon from '@material-ui/icons/ThumbUp';
import dayjs from 'dayjs';
import withStyles from '@material-ui/core/styles/withStyles';
import React from 'react';
import Button from './TooltipIconButton';

import firebase from 'firebase/app';
import { db } from '../firebase';


const styles = (theme) => ({
  card: {
    display: 'flex',
    minHeight: 200,
  },
  thumbnail: {
    minWidth: 200,
  },
  content: {
    padding: 25,
    objectFit: 'cover',
  },
});

class MapCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = { unsubscribeUpvote: undefined };
    this.upvote = this.upvote.bind(this);
  }

  componentDidMount() {
    const { map } = this.props;
    const unsubscribeUpvote = db.collection('map-votes').doc(map.mapId)
    .onSnapshot( doc => {
      this.setState({ upvote: doc.data().upvote });
    });
    this.setState({ unsubscribeUpvote });
  }

  componentWillUnmount() {
    this.state.unsubscribeUpvote && this.state.unsubscribeUpvote();
  }

  upvote() {
    const { map } = this.props;
    db.collection('map-votes').doc(map.mapId)
      .update({ upvote: firebase.firestore.FieldValue.increment(1) });
  }

  render() {
    const { 
      classes, 
      map: {
        mapId,
        thumbUrl,
        startAt,
        description,
        title,
      },
    } = this.props;

    return (
      <Card className={classes.card}>
        <CardMedia
          image={thumbUrl}
          title="Map thumbnail"
          className={classes.thumbnail}
        />
        <CardContent className={classes.content}>
          <Link to={`/maps/${mapId}`}>
            <Typography
              variant="h5"
              to={`/maps/${mapId}`}
              color="primary"
            >
              {title}
            </Typography>
          </Link>
          <Typography variant="body2" color="textSecondary">
            {startAt}
          </Typography>
          <Typography variant="body1">{description}</Typography>
          <Button onClick={this.upvote} tip="Click to +1">
            <FavoriteIcon color="primary" />
          </Button>
          <span>{this.state.upvote} UpVote</span>
        </CardContent>
      </Card>
    );
  }
}

export default withStyles(styles)(MapCard);