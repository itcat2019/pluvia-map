import React from 'react';
import { connect } from 'react-redux';
import { Typography, Grid, Button } from '@material-ui/core';
import { Marker as ReactGoogleMapMarker, InfoWindow } from 'react-google-maps';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { emojiIndex } from 'emoji-mart';
import { SHOW_INFOWINDOW, CLOSE_INFOWINDOW, START_EDIT_MARKER } from '../redux/reducers/screenReducer';
import { db } from '../firebase';

// represent Marker and the InfoWindow
class Marker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mapId: window.location.pathname.split('/').filter(p => p !== '').pop(),
    };
    this.deleteMarker = this.deleteMarker.bind(this);
  }

  deleteMarker() {
    const { marker } = this.props;
    const { mapId } = this.state;
    db.collection('maps').doc(mapId).collection('markers').doc(marker.markerId).delete()
      .then(() => console.log('marker deleted'))
      .catch((error) => console.error('marker delete error', error));
  }

  render() {
    const { marker } = this.props;
    let emojiIcon = null;
    if (marker.emojiIconId) {
      const foundEmoji = emojiIndex.search(marker.emojiIconId);
      if (foundEmoji.length > 0) {
        emojiIcon = foundEmoji[0];
      }
    }
    return (
      <ReactGoogleMapMarker key={marker.markerId} position={marker.latLng}
        label={emojiIcon ? { fontSize: '20px', text: emojiIcon.native } : null}
        icon=''
        onClick={() => this.props.editingMarker(marker.markerId)}
        onMouseOver={() => this.props.showInfoWindow(marker.markerId)}
      >
        {this.props.showMarkerId === marker.markerId &&
          <InfoWindow onCloseClick={this.props.closeInfoWindow}>
            <Grid>
              <Grid>
                <Typography variant="subtitle1">{marker.title || 'No Title'}</Typography>
                {marker.description}
              </Grid>
            </Grid>
          </InfoWindow>}
      </ReactGoogleMapMarker>
    )
  }
}

const mapStateToProps = (state) => ({
  showMarkerId: state.screen.map.showMarkerId,
});
const mapDispatchToProps = (dispatch) => ({
  showInfoWindow: (markerId) => dispatch({ type: SHOW_INFOWINDOW, markerId }),
  closeInfoWindow: () => dispatch({ type: CLOSE_INFOWINDOW }),
  editingMarker: (markerId) => dispatch({ type: START_EDIT_MARKER, markerId }),
});
export default connect(mapStateToProps, mapDispatchToProps)(Marker);