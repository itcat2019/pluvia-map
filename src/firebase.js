import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/performance';

import settings from './settings';

const firebaseApp = firebase.initializeApp(settings.credentials.firebase);

const auth = firebase.auth();
auth.useDeviceLanguage();

const db = firebaseApp.firestore();

// eslint-disable-next-line no-unused-vars
const performance = firebase.performance();

export { auth, db };
export default firebaseApp;